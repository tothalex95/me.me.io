/*
 * Demo data for testing the application
 */

-- Demo Users, the password is 'password' for all
INSERT INTO user (id, username, password)
VALUES ("1", "developer",
        "$2a$10$lgvEI7MS0CdI/flGoYxNgOMiWmcJNKgCEF4tyF9mlFdhjGNg7z9EK");
INSERT INTO user (id, username, password)
VALUES ("2", "alextoth",
        "$2a$10$lgvEI7MS0CdI/flGoYxNgOMiWmcJNKgCEF4tyF9mlFdhjGNg7z9EK");
INSERT INTO user (id, username, password)
VALUES ("3", "adamsatan",
        "$2a$10$lgvEI7MS0CdI/flGoYxNgOMiWmcJNKgCEF4tyF9mlFdhjGNg7z9EK");
INSERT INTO user (id, username, password)
VALUES ("4", "bencebogdandy",
        "$2a$10$lgvEI7MS0CdI/flGoYxNgOMiWmcJNKgCEF4tyF9mlFdhjGNg7z9EK");
INSERT INTO user (id, username, password)
VALUES ("5", "testuser1",
        "$2a$10$lgvEI7MS0CdI/flGoYxNgOMiWmcJNKgCEF4tyF9mlFdhjGNg7z9EK");
INSERT INTO user (id, username, password)
VALUES ("6", "testuser2",
        "$2a$10$lgvEI7MS0CdI/flGoYxNgOMiWmcJNKgCEF4tyF9mlFdhjGNg7z9EK");
INSERT INTO user (id, username, password)
VALUES ("7", "testuser3",
        "$2a$10$lgvEI7MS0CdI/flGoYxNgOMiWmcJNKgCEF4tyF9mlFdhjGNg7z9EK");

-- Demo Posts
INSERT INTO post (id, title, text, parent_id, author_id, created, last_modified, has_child, image_type)
VALUES ("1", "Transparent PNG", null, null, "1", NOW(), NOW(), false, "png");
INSERT INTO post (id, title, text, parent_id, author_id, created, last_modified, has_child, image_type)
VALUES ("2", "Normal PNG", "Das good shit.", null, "1", NOW(), NOW(), false, "png");
INSERT INTO post (id, title, text, parent_id, author_id, created,
                  last_modified, has_child, image_type)
VALUES ("3", "JPG", null, null, "1", NOW(), NOW(), false, "jpg");
INSERT INTO post (id, title, text, parent_id, author_id, created,
                  last_modified, has_child, image_type)
VALUES ("4", "Animated GIF", null, null, "1", NOW(), NOW(), false, "gif");
INSERT INTO post (id, title, text, parent_id, author_id, created, last_modified, has_child)
VALUES ("5",
        "Without Image",
        "Post without image content.",
        null, "1",
        NOW(), NOW(), true);
INSERT INTO post (id, title, text, parent_id, author_id, created, last_modified, has_child)
VALUES ("6", null,
        "Comment for post without image.",
        "5", "1",
        NOW(), NOW(), false);

-- Resources
INSERT INTO resource_entry (resource_key, resource_value) VALUES ("memeio.errors.username.empty", "The username cannot be empty.");
INSERT INTO resource_entry (resource_key, resource_value) VALUES ("memeio.errors.username.minLength", "The username is too short.");
INSERT INTO resource_entry (resource_key, resource_value) VALUES ("memeio.errors.username.maxLength", "The username is too long.");
INSERT INTO resource_entry (resource_key, resource_value) VALUES ("memeio.errors.password.empty", "The password cannot be empty.");
INSERT INTO resource_entry (resource_key, resource_value) VALUES ("memeio.errors.password.minLength", "The password is too short.");
INSERT INTO resource_entry (resource_key, resource_value) VALUES ("memeio.errors.password.maxLength", "The password is too long.");
INSERT INTO resource_entry (resource_key, resource_value) VALUES ("memeio.errors.password.pattern", "The password does not match a valid password pattern.");
INSERT INTO resource_entry (resource_key, resource_value) VALUES ("memeio.errors.email.empty", "The email address cannot be empty.");
INSERT INTO resource_entry (resource_key, resource_value) VALUES ("memeio.errors.email.pattern", "The email address does not match a valid email pattern.");
