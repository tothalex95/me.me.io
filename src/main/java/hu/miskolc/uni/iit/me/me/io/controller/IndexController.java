package hu.miskolc.uni.iit.me.me.io.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Basic controller to provide an index page for the application.
 * 
 * @author Alex Toth
 */
@Controller
public class IndexController {

	/**
	 * HTTP.GET method to return the index page of the application.
	 * 
	 * @return Name of the HTML file that describes the index page.
	 */
	@GetMapping(value = { "/", "/index" })
	public String index() {
		return "index";
	}

}
