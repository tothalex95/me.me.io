package hu.miskolc.uni.iit.me.me.io.user.dto.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import hu.miskolc.uni.iit.me.me.io.user.config.UserConfigurationProperties;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Validator for username strings.
 * 
 * @author Alex Toth
 */
@RequiredArgsConstructor(onConstructor_ = { @Autowired })
@Component
public class UsernameValidator implements Validator {

    private static final String RESOURCEKEY_PREFIX = "memeio.errors.username.";

    @NonNull
    private final UserConfigurationProperties userConfigurationProperties;

    /**
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class<?> clazz) {
        return String.class.equals(clazz);
    }

    /**
     * @see org.springframework.validation.Validator#validate(java.lang.Object,
     *      org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object target, Errors errors) {
        String username = (String) target;

        if (username == null || username.isEmpty()) {
            errors.reject(RESOURCEKEY_PREFIX + "empty");
            return;
        }

        if (username.length() < userConfigurationProperties.getUsernameMinLength()) {
            errors.reject(RESOURCEKEY_PREFIX + "minLength");
        }

        if (username.length() > userConfigurationProperties.getUsernameMaxLength()) {
            errors.reject(RESOURCEKEY_PREFIX + "maxLength");
        }
    }

}
