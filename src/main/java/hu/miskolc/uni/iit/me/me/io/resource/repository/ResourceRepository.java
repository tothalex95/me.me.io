package hu.miskolc.uni.iit.me.me.io.resource.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hu.miskolc.uni.iit.me.me.io.resource.model.ResourceEntry;

/**
 * JPA repository to access {@link ResourceEntry} data in the database.
 * 
 * @author Alex Toth
 */
@Repository
public interface ResourceRepository extends JpaRepository<ResourceEntry, String> {

}
