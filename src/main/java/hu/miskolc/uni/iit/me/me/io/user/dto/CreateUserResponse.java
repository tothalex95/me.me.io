package hu.miskolc.uni.iit.me.me.io.user.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Alex Toth
 */
@Data
@Accessors(chain = true)
public class CreateUserResponse {

    /**
     * Identifier of the registered user.
     */
    private Long id;

}
