package hu.miskolc.uni.iit.me.me.io.user.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import hu.miskolc.uni.iit.me.me.io.user.model.User;

/**
 * User Service
 *
 * @author iasatan
 * @author Alex Toth
 */
public interface UserService extends UserDetailsService {

    /**
     * Registers a new {@link User}.
     *
     * @param user The {@link User} to be registered.
     * @return The registered {@link User}.
     */
    User createUser(User user);

    /**
     * Bence Bogdandy Updates the password or email of the {@link User}
     * 
     * @param user the {@link User} to be updated.
     * @return the updated {@link User}
     */
    User updateUser(User user);
}
