package hu.miskolc.uni.iit.me.me.io.user.exception;

/**
 * Exception for not valid email addresses
 *
 * @author iasatan
 */
public class EmailAddressNotValidException extends RuntimeException {
    public EmailAddressNotValidException() {
    }

    public EmailAddressNotValidException(String message) {
        super(message);
    }

    public EmailAddressNotValidException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmailAddressNotValidException(Throwable cause) {
        super(cause);
    }

    public EmailAddressNotValidException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
