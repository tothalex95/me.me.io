package hu.miskolc.uni.iit.me.me.io.post.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Alex Toth
 */
@Data
@Accessors(chain = true)
public class GetPostsRequest {

}
