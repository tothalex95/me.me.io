package hu.miskolc.uni.iit.me.me.io.user.repository;

import hu.miskolc.uni.iit.me.me.io.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JPA repository to access User data in the database.
 *
 * @author iasatan
 * @author Alex Toth
 */
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

    User findUserByEmail(String email);

    boolean existsUserByUsername(String username);

    boolean existsUserByEmail(String email);


}
