package hu.miskolc.uni.iit.me.me.io.post.exception;

/**
 * Image Storage Exception
 * 
 * @author Alex Toth
 */
public class ImageStorageException extends RuntimeException {

	/**
	 * Automatically generated serial version UID.
	 */
	private static final long serialVersionUID = 88142251491868816L;

	public ImageStorageException() {
		super();
	}

	public ImageStorageException(String message) {
		super(message);
	}

	public ImageStorageException(Throwable cause) {
		super(cause);
	}

	public ImageStorageException(String message, Throwable cause) {
		super(message, cause);
	}

	public ImageStorageException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
