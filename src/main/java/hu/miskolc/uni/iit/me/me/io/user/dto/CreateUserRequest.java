package hu.miskolc.uni.iit.me.me.io.user.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Alex Toth
 */
@Data
@Accessors(chain = true)
public class CreateUserRequest {

    /**
     * Username of the user.
     */
    private String username;

    /**
     * Password of the user.
     */
    private String password;

    /**
     * Email address of the user.
     */
    private String email;

}
