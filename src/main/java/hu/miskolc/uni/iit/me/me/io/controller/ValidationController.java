package hu.miskolc.uni.iit.me.me.io.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import hu.miskolc.uni.iit.me.me.io.exception.ValidationException;
import hu.miskolc.uni.iit.me.me.io.resource.service.ResourceService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Controller to catch and map validation related exceptions.
 * 
 * @author Alex Toth
 */
@RequiredArgsConstructor(onConstructor_ = { @Autowired })
@ControllerAdvice
public class ValidationController {

    @NonNull
    private final ResourceService resourceService;

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<?> handleValidationException(ValidationException validationException) {
        Map<String, List<String>> errors = new HashMap<>();
        errors.put("errors", validationException.getErrors()
                .stream()
                .map(errorCode -> resourceService.resolve(errorCode))
                .collect(Collectors.toList()));
        return ResponseEntity.badRequest().body(errors);
    }

}
