package hu.miskolc.uni.iit.me.me.io.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import hu.miskolc.uni.iit.me.me.io.user.exception.EmailAddresTakenException;
import hu.miskolc.uni.iit.me.me.io.user.exception.UsernameTakenException;
import hu.miskolc.uni.iit.me.me.io.user.model.User;
import hu.miskolc.uni.iit.me.me.io.user.repository.UserRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * User Service implementation.
 *
 * @author iasatan
 * @author Alex Toth
 */
@RequiredArgsConstructor(onConstructor_ = { @Autowired })
@Service
public class UserServiceImpl implements UserService {

    @NonNull
    private final PasswordEncoder passwordEncoder;

    @NonNull
    private final UserRepository userRepository;

    /**
     * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
     */
    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return user;
    }

    /**
     * @see hu.miskolc.uni.iit.me.me.io.user.service.UserService#createUser(hu.miskolc.uni.iit.me.me.io.user.model.User)
     */
    @Override
    public User createUser(User user) {
        if (userRepository.existsUserByUsername(user.getUsername())) {
            throw new UsernameTakenException();
        }
        checkEmail(user);
        return userRepository.saveAndFlush(user.setPassword(passwordEncoder.encode(user.getPassword())));

    }

    /**
     * @see hu.miskolc.uni.iit.me.me.io.user.service.UserService#updateUser(hu.miskolc.uni.iit.me.me.io.user.model.User)
     */
    @Override
    public User updateUser(User user) {
        User actualUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        checkEmail(user);
        if (user.getPassword() != null) {
            actualUser.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        if (user.getEmail() != null) {
            actualUser.setEmail(user.getEmail());
        }
        return userRepository.saveAndFlush(actualUser);
    }

    /**
     * Checks the availability of provided email
     *
     * @param user data sent to the server
     */
    private void checkEmail(User user) {
        if (user.getEmail() != null && userRepository.existsUserByEmail(user.getEmail())) {
            throw new EmailAddresTakenException();
        }
    }

}
