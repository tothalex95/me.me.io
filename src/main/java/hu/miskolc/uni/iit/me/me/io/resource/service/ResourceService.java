package hu.miskolc.uni.iit.me.me.io.resource.service;

import hu.miskolc.uni.iit.me.me.io.resource.model.ResourceEntry;

/**
 * @author Alex Toth
 */
public interface ResourceService {

    /**
     * Retrieve the resolved value of the {@link ResourceEntry} identified by the
     * given key.
     * 
     * @param key The identifier of the requested resource.
     * @return The resolved value of the requested resource.
     */
    String resolve(String key);

}
