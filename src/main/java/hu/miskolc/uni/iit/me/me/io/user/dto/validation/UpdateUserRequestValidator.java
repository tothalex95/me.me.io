package hu.miskolc.uni.iit.me.me.io.user.dto.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import hu.miskolc.uni.iit.me.me.io.user.dto.UpdateUserRequest;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Validate fields of {@link UpdateUserRequest}.
 * 
 * @author Alex Toth
 */
@RequiredArgsConstructor(onConstructor_ = { @Autowired })
@Component
public class UpdateUserRequestValidator implements Validator {

    @NonNull
    private final PasswordValidator passwordValidator;

    @NonNull
    private final EmailValidator emailValidator;

    /**
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class<?> clazz) {
        return UpdateUserRequest.class.equals(clazz);
    }

    /**
     * @see org.springframework.validation.Validator#validate(java.lang.Object,
     *      org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object target, Errors errors) {
        UpdateUserRequest updateUserRequest = (UpdateUserRequest) target;

        if (updateUserRequest.getPassword() != null) {
            passwordValidator.validate(updateUserRequest.getPassword(), errors);
        }

        if (updateUserRequest.getEmail() != null) {
            emailValidator.validate(updateUserRequest.getEmail(), errors);
        }
    }

}
