package hu.miskolc.uni.iit.me.me.io.post.dto.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import hu.miskolc.uni.iit.me.me.io.post.dto.CreatePostRequest;

/**
 * Validate fields of {@link CreatePostRequest}.
 * 
 * @author Alex Toth
 */
@Component
public class CreatePostRequestValidator implements Validator {

    private static final String RESOURCEKEY_PREFIX = "memeio.errors.";

    /**
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class<?> clazz) {
        return CreatePostRequest.class.equals(clazz);
    }

    /**
     * @see org.springframework.validation.Validator#validate(java.lang.Object,
     *      org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object target, Errors errors) {
        CreatePostRequest createPostRequest = (CreatePostRequest) target;

        String title = createPostRequest.getTitle();
        Long parentId = createPostRequest.getParentId();

        if ((title == null && parentId == null) || (title != null && parentId != null)) {
            errors.reject(RESOURCEKEY_PREFIX + "invalidCreatePostRequest");
        }
    }

}
