package hu.miskolc.uni.iit.me.me.io.post.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import hu.miskolc.uni.iit.me.me.io.user.model.User;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Model class of posts.
 *
 * @author iasatan
 * @author Alex Toth
 */
@Data
@Accessors(chain = true)
@Entity
public class Post {

    /**
     * Automatically generated identifier of the post.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Author of the post.
     */
    @ManyToOne
    private User author;

    /**
     * Title of the post. In case of comments it should be null.
     */
    @Column
    private String title;

    /**
     * Text content of the post.
     */
    @Column
    private String text;

    /**
     * If the parent is not null, that means the post is actually a comment.
     */
    @Column
    private Long parentId;

    /**
     * Time of creation of the post.
     */
    @Column(nullable = false)
    private LocalDateTime created;

    /**
     * Time of last modification of the post.
     */
    @Column(nullable = false)
    private LocalDateTime lastModified;

    /**
     * Stores if the post has comments
     */
    @Column
    private Boolean hasChild;

    /**
     * Extension of the corresponding image, or null if the post doesn't have image
     * content.
     */
    @Column(nullable = true)
    private String imageType;

}
