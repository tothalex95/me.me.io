package hu.miskolc.uni.iit.me.me.io.user.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Alex Toth
 */
@Data
@Accessors(chain = true)
public class UpdateUserRequest {

    /**
     * The new password of the user.
     */
    private String password;

    /**
     * The new email address of the user.
     */
    private String email;

}
