package hu.miskolc.uni.iit.me.me.io.post.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Alex Toth
 */
@Data
@Accessors(chain = true)
public class CreatePostRequest {

    /**
     * Title of the post.
     */
    private String title;

    /**
     * Text content of the post.
     */
    private String text;

    /**
     * Identifier of the comment's parent or null if it's a normal post.
     */
    private Long parentId;

}
