package hu.miskolc.uni.iit.me.me.io.post.config;

import javax.validation.constraints.NotBlank;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * Post related configuration properties.
 * 
 * @author Alex Toth
 */
@Getter
@Setter
@Component
@PropertySource("classpath:post-configuration.properties")
@ConfigurationProperties(prefix = "post")
public class PostConfigurationProperties {

	@NotBlank
	private String imageStorageLocation;

}
