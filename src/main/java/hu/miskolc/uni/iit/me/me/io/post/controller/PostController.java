package hu.miskolc.uni.iit.me.me.io.post.controller;

import java.lang.reflect.Type;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FilenameUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import hu.miskolc.uni.iit.me.me.io.exception.ValidationException;
import hu.miskolc.uni.iit.me.me.io.post.dto.CreatePostRequest;
import hu.miskolc.uni.iit.me.me.io.post.dto.CreatePostResponse;
import hu.miskolc.uni.iit.me.me.io.post.dto.GetCommentsRequest;
import hu.miskolc.uni.iit.me.me.io.post.dto.GetCommentsResponse;
import hu.miskolc.uni.iit.me.me.io.post.dto.GetPostRequest;
import hu.miskolc.uni.iit.me.me.io.post.dto.GetPostResponse;
import hu.miskolc.uni.iit.me.me.io.post.dto.GetPostsRequest;
import hu.miskolc.uni.iit.me.me.io.post.dto.GetPostsResponse;
import hu.miskolc.uni.iit.me.me.io.post.dto.validation.CreatePostRequestValidator;
import hu.miskolc.uni.iit.me.me.io.post.exception.ImageNotFoundException;
import hu.miskolc.uni.iit.me.me.io.post.model.Post;
import hu.miskolc.uni.iit.me.me.io.post.service.ImageStorageService;
import hu.miskolc.uni.iit.me.me.io.post.service.PostService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Controller for actions related to creating, editing, listing posts.
 *
 * @author Alex Toth
 */
@RequiredArgsConstructor(onConstructor_ = { @Autowired })
@RestController
@RequestMapping("/posts")
public class PostController {

    /**
     * Filename template for saved post images.
     */
    private static final String POST_IMAGE_FILENAME_TEMPLATE = "POST_%d.%s";

    private static final Type GETPOSTRESPONSE_LIST_TYPE = new TypeToken<List<GetPostResponse>>() {
    }.getType();

    @NonNull
    private final ModelMapper modelMapper;

    @NonNull
    private final PostService postService;

    @NonNull
    private final ImageStorageService imageStorageService;

    @NonNull
    private final CreatePostRequestValidator createPostRequestValidator;

    @PostConstruct
    public void init() {
        modelMapper.typeMap(CreatePostRequest.class, Post.class).addMappings(mapper -> mapper.skip(Post::setId));
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<CreatePostResponse> createPost(
            @RequestPart(value = "post", required = true) CreatePostRequest createPostRequest,
            @RequestPart(value = "image", required = false) MultipartFile image, BindingResult bindingResult) {
        createPostRequestValidator.validate(createPostRequest, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult.getAllErrors()
                    .stream()
                    .map(error -> error.getCode())
                    .collect(Collectors.toList()));
        }

        Post post = modelMapper.map(createPostRequest, Post.class);

        post.setImageType(image == null ? null : FilenameUtils.getExtension(image.getOriginalFilename()));
        post = postService.createPost(post);

        if (image != null) {
            String filename = String.format(POST_IMAGE_FILENAME_TEMPLATE, post.getId(), post.getImageType());

            imageStorageService.store(filename, image);
        }

        CreatePostResponse createPostResponse = modelMapper.map(post, CreatePostResponse.class);

        URI uri = UriComponentsBuilder.newInstance().path("/posts/{id}").build(createPostResponse.getId());

        return ResponseEntity.created(uri).body(createPostResponse);
    }

    @GetMapping
    public ResponseEntity<GetPostsResponse> getPosts(GetPostsRequest getPostsRequest) {
        List<Post> posts = postService.getLastTenPost();

        GetPostsResponse getPostsResponse = new GetPostsResponse()
                .setPosts(modelMapper.map(posts, GETPOSTRESPONSE_LIST_TYPE));

        return ResponseEntity.ok(getPostsResponse);
    }

    @GetMapping("/{postId}")
    public ResponseEntity<GetPostResponse> getPost(@PathVariable("postId") Long id, GetPostRequest getPostRequest) {
        Post post = postService.getPostById(id);

        GetPostResponse getPostResponse = modelMapper.map(post, GetPostResponse.class);

        return ResponseEntity.ok(getPostResponse);
    }

    @GetMapping("{postId}/comments")
    public ResponseEntity<GetCommentsResponse> getCommentForPost(@PathVariable("postId") Long id,
            GetCommentsRequest getCommentsRequest) {
        List<Post> comments = postService.getCommentsByPost(id);

        GetCommentsResponse getCommentsResponse = new GetCommentsResponse()
                .setComments(modelMapper.map(comments, GETPOSTRESPONSE_LIST_TYPE));

        return ResponseEntity.ok(getCommentsResponse);
    }

    @GetMapping(value = "/{postId}/image", produces = { MediaType.IMAGE_GIF_VALUE, MediaType.IMAGE_JPEG_VALUE,
            MediaType.IMAGE_PNG_VALUE })
    public ResponseEntity<Resource> getPostImage(@PathVariable("postId") Long id) {
        String imageType = postService.getPostById(id).getImageType();
        if (imageType == null) {
            throw new ImageNotFoundException();
        }

        String filename = String.format(POST_IMAGE_FILENAME_TEMPLATE, id, imageType);

        Resource resource = imageStorageService.load(filename);

        return ResponseEntity.ok(resource);
    }

    /**
     * Return HttpStatus.NOT_FOUND when {@link ImageNotFoundException} is thrown
     * instead of internal server error message.
     *
     * @return HTTP 404
     */
    @ExceptionHandler(ImageNotFoundException.class)
    public ResponseEntity<?> imageNotFoundHandler() {
        return ResponseEntity.notFound().build();
    }

}
