package hu.miskolc.uni.iit.me.me.io.user.exception;

/**
 * Exception for already existing usernames
 *
 * @author iasatan
 */
public class UsernameTakenException extends RuntimeException {
    public UsernameTakenException() {
    }

    public UsernameTakenException(String message) {
        super(message);
    }

    public UsernameTakenException(String message, Throwable cause) {
        super(message, cause);
    }

    public UsernameTakenException(Throwable cause) {
        super(cause);
    }

    public UsernameTakenException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
