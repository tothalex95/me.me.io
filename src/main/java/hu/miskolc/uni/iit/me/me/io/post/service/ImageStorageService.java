package hu.miskolc.uni.iit.me.me.io.post.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

/**
 * Image Storage Service
 * 
 * @author Alex Toth
 */
public interface ImageStorageService {

	/**
	 * Store the given image on the file system or in the database.
	 * 
	 * @param filename The name of the image to be stored.
	 * @param image    The image to be stored.
	 */
	void store(String filename, MultipartFile image);

	/**
	 * Load the image with the given filename from the file system or the database.
	 * 
	 * @param filename The name of the image to be loaded.
	 * @return The loaded image as a {@link Resource}.
	 */
	Resource load(String filename);
}
