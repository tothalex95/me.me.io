package hu.miskolc.uni.iit.me.me.io.post.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hu.miskolc.uni.iit.me.me.io.post.model.Post;

/**
 * JPA repository to access Post data in the database.
 *
 * @author iasatan
 * @author Alex Toth
 */
@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    List<Post> findTop10ByParentIdIsNullOrderByIdDesc();

    List<Post> getPostsByParentIdIs(long id);

}
