package hu.miskolc.uni.iit.me.me.io.user.exception;

/**
 * Exception for already existing email addresses
 *
 * @author iasatan
 */
public class EmailAddresTakenException extends RuntimeException {
    public EmailAddresTakenException() {
    }

    public EmailAddresTakenException(String message) {
        super(message);
    }

    public EmailAddresTakenException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmailAddresTakenException(Throwable cause) {
        super(cause);
    }

    public EmailAddresTakenException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
