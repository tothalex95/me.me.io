package hu.miskolc.uni.iit.me.me.io.user.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Alex Toth
 */
@Data
@Accessors(chain = true)
public class UpdateUserResponse {

    /**
     * Identifier of the user. This is just a placeholder here, if a new property is
     * added, it should be deleted.
     */
    private Long id;

}
