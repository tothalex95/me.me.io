package hu.miskolc.uni.iit.me.me.io.resource.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hu.miskolc.uni.iit.me.me.io.resource.service.ResourceService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Controller for resolving resource codes.
 * 
 * @author Alex Toth
 */
@RequiredArgsConstructor(onConstructor_ = { @Autowired })
@RestController
@RequestMapping("/resource")
public class ResourceController {

    @NonNull
    private final ResourceService resourceService;

    @GetMapping("/{key:.+}")
    public ResponseEntity<String> resolve(@PathVariable String key) {
        return ResponseEntity.ok(resourceService.resolve(key));
    }

}
