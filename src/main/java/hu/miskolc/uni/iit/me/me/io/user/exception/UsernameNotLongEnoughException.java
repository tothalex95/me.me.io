package hu.miskolc.uni.iit.me.me.io.user.exception;

/**
 * Exception for too short usernames
 *
 * @author iasatan
 */
public class UsernameNotLongEnoughException extends RuntimeException {
    public UsernameNotLongEnoughException() {
    }

    public UsernameNotLongEnoughException(String message) {
        super(message);
    }

    public UsernameNotLongEnoughException(String message, Throwable cause) {
        super(message, cause);
    }

    public UsernameNotLongEnoughException(Throwable cause) {
        super(cause);
    }

    public UsernameNotLongEnoughException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
