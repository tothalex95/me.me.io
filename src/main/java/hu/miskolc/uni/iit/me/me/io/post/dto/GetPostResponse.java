package hu.miskolc.uni.iit.me.me.io.post.dto;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Alex Toth
 */
@Data
@Accessors(chain = true)
public class GetPostResponse {

    /**
     * Identifier of the post.
     */
    private Long id;

    /**
     * Title of the post.
     */
    private String title;

    /**
     * Text content of the post.
     */
    private String text;

    /**
     * Time of the post's creation.
     */
    private LocalDateTime created;

    /**
     * Time of the post's last modification.
     */
    private LocalDateTime lastModified;

    /**
     * Identifier of the comment's parent or null if it's a normal post.
     */
    private Long parentId;

    /**
     * Whether the post has related comments or not.
     */
    private Boolean hasChild;

    /**
     * Type of the post's image content or null if the post hasn't got image
     * content.
     */
    private String imageType;

    /**
     * Username of the post's author.
     */
    private String authorUsername;

}
