package hu.miskolc.uni.iit.me.me.io.resource.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.miskolc.uni.iit.me.me.io.resource.model.ResourceEntry;
import hu.miskolc.uni.iit.me.me.io.resource.repository.ResourceRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * @author Alex Toth
 */
@RequiredArgsConstructor(onConstructor_ = { @Autowired })
@Service
public class ResourceServiceImpl implements ResourceService {

    @NonNull
    private final ResourceRepository resourceRepository;

    /**
     * @see hu.miskolc.uni.iit.me.me.io.resource.service.ResourceService#resolve(java.lang.String)
     */
    @Override
    public String resolve(String key) {
        return resourceRepository.findById(key)
                .orElse(new ResourceEntry().setResourceValue("Resource unresolved: " + key)).getResourceValue();
    }

}
