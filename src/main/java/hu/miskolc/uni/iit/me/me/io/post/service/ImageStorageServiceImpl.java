package hu.miskolc.uni.iit.me.me.io.post.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import hu.miskolc.uni.iit.me.me.io.post.config.PostConfigurationProperties;
import hu.miskolc.uni.iit.me.me.io.post.exception.ImageNotFoundException;
import hu.miskolc.uni.iit.me.me.io.post.exception.ImageStorageException;
import lombok.RequiredArgsConstructor;

/**
 * Image Storage Service implementation.
 * 
 * @author Alex Toth
 */
@RequiredArgsConstructor(onConstructor_ = { @Autowired })
@Service
public class ImageStorageServiceImpl implements ImageStorageService {

	@NonNull
	private final PostConfigurationProperties postConfigurationProperties;

	private Path imageStorageLocation;

	/**
	 * Initialize properties that can not be set automatically by auto wiring.
	 */
	@PostConstruct
	public void initialize() {
		imageStorageLocation = Paths.get(postConfigurationProperties.getImageStorageLocation());
	}

	/**
	 * @see hu.miskolc.uni.iit.me.me.io.post.service.ImageStorageService#store(java.lang.String,
	 *      org.springframework.web.multipart.MultipartFile)
	 */
	@Override
	public void store(String filename, MultipartFile image) {
		try (InputStream inputStream = image.getInputStream()) {
			Files.copy(inputStream, imageStorageLocation.resolve(filename), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			throw new ImageStorageException(String.format("Failed to save image: %s", image.getOriginalFilename()), e);
		}
	}

	/**
	 * @see hu.miskolc.uni.iit.me.me.io.post.service.ImageStorageService#load(java.lang.String)
	 */
	@Override
	public Resource load(String filename) {
		try {
			Path file = imageStorageLocation.resolve(filename);
			Resource resource = new UrlResource(file.toUri());

			if (resource.exists() && resource.isReadable()) {
				return resource;
			}

			throw new ImageNotFoundException(String.format("Failed to load image: %s", filename));
		} catch (MalformedURLException e) {
			throw new ImageNotFoundException(String.format("Failed to load image: %s", filename), e);
		}
	}

}
