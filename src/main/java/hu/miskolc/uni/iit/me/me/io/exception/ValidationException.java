package hu.miskolc.uni.iit.me.me.io.exception;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Exception to throw when DTO validation error happens.
 * 
 * @author Alex Toth
 */
@AllArgsConstructor
public class ValidationException extends RuntimeException {

    /**
     * Auto-generated serial version UID.
     */
    private static final long serialVersionUID = -688464325258983598L;

    /**
     * List of error codes.
     */
    @Getter
    private final List<String> errors;

}
