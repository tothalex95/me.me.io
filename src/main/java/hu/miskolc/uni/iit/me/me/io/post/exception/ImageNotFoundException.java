package hu.miskolc.uni.iit.me.me.io.post.exception;

/**
 * Image Not Found Exception.
 * 
 * @author Alex Toth
 */
public class ImageNotFoundException extends RuntimeException {

	/**
	 * Automatically generated serial version UID.
	 */
	private static final long serialVersionUID = -1754973140537623528L;

	public ImageNotFoundException() {
		super();
	}

	public ImageNotFoundException(String message) {
		super(message);
	}

	public ImageNotFoundException(Throwable cause) {
		super(cause);
	}

	public ImageNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public ImageNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
