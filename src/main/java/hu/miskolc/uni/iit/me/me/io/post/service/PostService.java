package hu.miskolc.uni.iit.me.me.io.post.service;

import hu.miskolc.uni.iit.me.me.io.post.model.Post;

import java.util.List;

/**
 * Post Service
 *
 * @author iasatan
 * @author Alex Toth
 */
public interface PostService {

    /**
     * Saves the post passed as a parameter.
     *
     * @param post The post to be saved.
     * @return The saved post with its identifier.
     */
    Post createPost(Post post);

    /**
     * Returns the latest 10 posts avaiable in the database
     *
     * @return The posts without comments
     */
    List<Post> getLastTenPost();

    /**
     * Returns a post with a matching id
     *
     * @param id tp identify the post
     * @return the post with the id
     */
    Post getPostById(long id);

    /**
     * Returns the comments to the specified Post
     *
     * @param id of the post
     * @return the comments related to the post
     */
    List<Post> getCommentsByPost(long id);

}
