package hu.miskolc.uni.iit.me.me.io.post.dto;

import java.util.List;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Alex Toth
 */
@Data
@Accessors(chain = true)
public class GetPostsResponse {

    /**
     * List of posts retrieved from the database.
     */
    private List<GetPostResponse> posts;

}
