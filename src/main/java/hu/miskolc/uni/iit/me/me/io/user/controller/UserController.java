package hu.miskolc.uni.iit.me.me.io.user.controller;

import java.net.URI;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import hu.miskolc.uni.iit.me.me.io.exception.ValidationException;
import hu.miskolc.uni.iit.me.me.io.user.dto.CreateUserRequest;
import hu.miskolc.uni.iit.me.me.io.user.dto.CreateUserResponse;
import hu.miskolc.uni.iit.me.me.io.user.dto.UpdateUserRequest;
import hu.miskolc.uni.iit.me.me.io.user.dto.UpdateUserResponse;
import hu.miskolc.uni.iit.me.me.io.user.dto.validation.CreateUserRequestValidator;
import hu.miskolc.uni.iit.me.me.io.user.dto.validation.UpdateUserRequestValidator;
import hu.miskolc.uni.iit.me.me.io.user.model.User;
import hu.miskolc.uni.iit.me.me.io.user.service.UserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Controller for user related actions.
 *
 * @author iasatan
 * @author Alex Toth
 */
@RequiredArgsConstructor(onConstructor_ = { @Autowired })
@RestController
@RequestMapping("/users")
public class UserController {

    @NonNull
    private final ModelMapper modelMapper;

    @NonNull
    private final UserService userService;

    @NonNull
    private final CreateUserRequestValidator createUserRequestValidator;

    @NonNull
    private final UpdateUserRequestValidator updateUserRequestValidator;

    @PostMapping
    public ResponseEntity<CreateUserResponse> createUser(@RequestBody CreateUserRequest createUserRequest,
            BindingResult bindingResult) {
        createUserRequestValidator.validate(createUserRequest, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult.getAllErrors()
                    .stream()
                    .map(error -> error.getCode())
                    .collect(Collectors.toList()));
        }

        User user = modelMapper.map(createUserRequest, User.class);

        user = userService.createUser(user);

        CreateUserResponse createUserResponse = modelMapper.map(user, CreateUserResponse.class);

        URI uri = UriComponentsBuilder.newInstance().path("/users/{id}").build(createUserResponse.getId());

        return ResponseEntity.created(uri).body(createUserResponse);
    }

    @PutMapping
    public ResponseEntity<UpdateUserResponse> updateUser(@RequestBody UpdateUserRequest updateUserRequest,
            BindingResult bindingResult) {
        updateUserRequestValidator.validate(updateUserRequest, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult.getAllErrors()
                    .stream()
                    .map(error -> error.getCode())
                    .collect(Collectors.toList()));
        }

        User user = modelMapper.map(updateUserRequest, User.class);

        user = userService.updateUser(user);

        UpdateUserResponse updateUserResponse = modelMapper.map(user, UpdateUserResponse.class);

        return ResponseEntity.ok(updateUserResponse);
    }

}
