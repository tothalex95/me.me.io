package hu.miskolc.uni.iit.me.me.io.post.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hu.miskolc.uni.iit.me.me.io.post.model.Post;
import hu.miskolc.uni.iit.me.me.io.post.repository.PostRepository;
import hu.miskolc.uni.iit.me.me.io.user.model.User;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Post Service implementation.
 *
 * @author Alex Toth
 */
@RequiredArgsConstructor(onConstructor_ = { @Autowired })
@Service
public class PostServiceImpl implements PostService {

    @NonNull
    private final PostRepository postRepository;

    /**
     * @see hu.miskolc.uni.iit.me.me.io.post.service.PostService#createPost(hu.miskolc.uni.iit.me.me.io.post.model.Post)
     */
    @Transactional
    @Override
    public Post createPost(Post post) {
        User author = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        LocalDateTime now = LocalDateTime.now();

        post.setAuthor(author).setCreated(now).setLastModified(now);

        if (post.getParentId() != null) {
            Post parent = postRepository.getOne(post.getParentId());

            parent.setHasChild(true);

            postRepository.save(parent);
        }

        return postRepository.saveAndFlush(post);
    }

    /**
     * Returns the latest 10 posts avaiable in the database
     *
     * @return The posts without comments
     */
    @Override
    public List<Post> getLastTenPost() {
        return postRepository.findTop10ByParentIdIsNullOrderByIdDesc();
    }

    /**
     * Returns a post with a matching id
     *
     * @param id tp identify the post
     * @return the post with the id
     */
    @Override
    public Post getPostById(long id) {
        return postRepository.getOne(id);
    }

    /**
     * Returns the comments to the specified Post
     *
     * @param id of the post
     * @return the comments related to the post
     */
    @Override
    public List<Post> getCommentsByPost(long id) {
        return postRepository.getPostsByParentIdIs(id);
    }

}
