package hu.miskolc.uni.iit.me.me.io.user.dto.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import hu.miskolc.uni.iit.me.me.io.user.config.UserConfigurationProperties;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Validator for email address strings.
 * 
 * @author Alex Toth
 */
@RequiredArgsConstructor(onConstructor_ = { @Autowired })
@Component
public class EmailValidator implements Validator {

    private static final String RESOURCEKEY_PREFIX = "memeio.errors.email.";

    @NonNull
    private final UserConfigurationProperties userConfigurationProperties;

    /**
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class<?> clazz) {
        return String.class.equals(clazz);
    }

    /**
     * @see org.springframework.validation.Validator#validate(java.lang.Object,
     *      org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object target, Errors errors) {
        String email = (String) target;

        if (email == null || email.isEmpty()) {
            errors.reject(RESOURCEKEY_PREFIX + "empty");
            return;
        }

        if (!email.matches(userConfigurationProperties.getEmailPattern())) {
            errors.reject(RESOURCEKEY_PREFIX + "pattern");
        }
    }

}
