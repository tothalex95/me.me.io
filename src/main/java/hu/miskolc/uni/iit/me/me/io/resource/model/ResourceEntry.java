package hu.miskolc.uni.iit.me.me.io.resource.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Model class of resource entries.
 * 
 * @author Alex Toth
 */
@Data
@Accessors(chain = true)
@Entity
public class ResourceEntry {

    /**
     * Identifier of the resource entry.
     */
    @Id
    private String resourceKey;

    /**
     * Value of the resource entry.
     */
    private String resourceValue;

}
