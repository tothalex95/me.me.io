package hu.miskolc.uni.iit.me.me.io.user.config;

import javax.validation.constraints.NotBlank;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * User related configuration properties.
 * 
 * @author Alex Toth
 */
@Getter
@Setter
@Component
@PropertySource("classpath:user-configuration.properties")
@ConfigurationProperties(prefix = "user")
public class UserConfigurationProperties {

    private int usernameMinLength;

    private int usernameMaxLength;

    private int passwordMinLength;

    private int passwordMaxLength;

    @NotBlank
    private String passwordPattern;

    @NotBlank
    private String emailPattern;

}
