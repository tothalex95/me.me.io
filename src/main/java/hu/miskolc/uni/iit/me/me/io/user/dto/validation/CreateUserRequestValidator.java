package hu.miskolc.uni.iit.me.me.io.user.dto.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import hu.miskolc.uni.iit.me.me.io.user.dto.CreateUserRequest;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Validate fields of {@link CreateUserRequest}.
 * 
 * @author Alex Toth
 */
@RequiredArgsConstructor(onConstructor_ = { @Autowired })
@Component
public class CreateUserRequestValidator implements Validator {

    @NonNull
    private final UsernameValidator usernameValidator;

    @NonNull
    private final PasswordValidator passwordValidator;

    @NonNull
    private final EmailValidator emailValidator;

    /**
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class<?> clazz) {
        return CreateUserRequest.class.equals(clazz);
    }

    /**
     * @see org.springframework.validation.Validator#validate(java.lang.Object,
     *      org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object target, Errors errors) {
        CreateUserRequest createUserRequest = (CreateUserRequest) target;

        usernameValidator.validate(createUserRequest.getUsername(), errors);
        passwordValidator.validate(createUserRequest.getPassword(), errors);
        emailValidator.validate(createUserRequest.getEmail(), errors);
    }

}
