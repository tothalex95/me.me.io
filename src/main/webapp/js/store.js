/**
 * Vuex Store
 * 
 * Store shared objects used by components jointly.
 */
const store = new Vuex.Store({
    state: {
        authenticated: false
    },
    mutations: {
        /**
         * Set authenticated value to true.
         */
        authenticate: state => state.authenticated = true,

        /**
         * Set authenticated value to false.
         */
        logout: state => state.authenticated = false
    },
    getters: {
        /**
         * Property to access authenticated value.
         */
        isAuthenticated: state => state.authenticated
    },
    actions: {
        /**
         * Set authenticated flag to true.
         * 
         * @param {*} commit Part of state context.
         */
        authenticate({
            commit
        }) {
            commit("authenticate")
        },

        /**
         * Set authenticated flag to false.
         * 
         * @param {*} commit Part of state context.
         */
        logout({
            commit
        }) {
            commit("logout")
        }
    },
    plugins: [
        /**
         * Vuex Persistance Plugin to prevent forgetting state on refresh.
         */
        new window.VuexPersistence.VuexPersistence().plugin
    ]
})