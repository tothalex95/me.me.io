/**
 * Router configuration
 * 
 * If user is not authenticated, navigate to login page.
 * Unknown paths are redirected to /.
 */
const router = new VueRouter({
    routes: [{
            path: "/",
            redirect: "/auth/login"
        },
        AuthRoute,
        PostRoute,
        UserRoute,
        NotFoundRoute, {
            path: "*",
            redirect: "/404"
        }
    ]
})