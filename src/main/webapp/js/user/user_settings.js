/**
 * User Settings Component
 */
const UserSettings = {
    data() {
        return {
            user: {
                password: "",
                confirmPassword: "",
                email: ""
            },
            messages: []
        }
    },
    computed: {
        isDirty() {
            return this.user.password || this.user.confirmPassword || this.user.email
        }
    },
    methods: {
        /**
         * Called when the form is submitted.
         */
        onSubmit() {
            // Validate input
            this.messages = []
            if (this.user.password !== this.user.confirmPassword) {
                this.messages.push({
                    text: "Passwords don't match.",
                    type: "error"
                })
            }
            if (!this.isDirty) {
                this.messages.push({
                    text: "Every field is empty.",
                    type: "error"
                })
            }

            if (this.messages.length > 0) {
                return
            }

            // Send HTTP PUT request to update user data.
            axios.put("/users", {
                    password: this.user.password || null,
                    email: this.user.email || null
                })
                .then(response => {
                    this.messages.push({
                        text: "Successfully updated user data.",
                        type: "success"
                    })
                })
                .catch(response => {
                    response.response.data.errors.forEach(errorCode => {
                        this.messages.push({
                            text: errorCode,
                            type: "error"
                        })
                    })
                })
            
            // Clear the form
            this.onClearButtonClick()
        },
        /**
         * Called when clear button is clicked.
         */
        onClearButtonClick() {
            this.user = {
                password: "",
                confirmPassword: "",
                email: ""
            }
            this.messages = []
        }
    },
    template: `
        <div class="container">
            <div v-if="messages.length > 0">
                <div
                    v-for="message in messages"
                    :class="{ alert: true, 'alert-success': message.type === 'success', 'alert-danger': message.type === 'error' }">
                    {{ message.text }}
                </div>
            </div>
            <form @submit.prevent="onSubmit()">
                <div class="form-group row">
                    <label for="password" class="col-form-label">Password</label>
                    <input type="password" id="password" class="form-control" v-model="user.password" minlength="8"
                        maxlength="16">
                </div>

                <div class="form-group row">
                    <label for="confirmPassword" class="col-form-label">Confirm password</label>
                    <input type="password" id="confirmPassword" class="form-control" v-model="user.confirmPassword" 
                        minlength="8" maxlength="16">
                </div>

                <div class="form-group row">
                    <label for="email" class="col-form-label">Email</label>
                    <input type="email" id="email" class="form-control" v-model="user.email">
                </div>

                <button type="submit" class="btn btn-primary" :disabled="!isDirty">Save</button>
                <button type="button" class="btn btn-warning" @click="onClearButtonClick" :disabled="!isDirty">Clear</button>
            </form>
        </div>
    `
}

/**
 * Router configuration of user settings component
 */
const UserSettingsRoute = {
    path: "settings",
    name: "user_settings",
    component: UserSettings
}