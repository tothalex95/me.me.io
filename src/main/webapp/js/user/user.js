/**
 * User Component
 */
const User = {
    template: `
        <div class="container">
            <router-view></router-view>
        </div>
    `
}

/**
 * Router configuration of user component
 */
const UserRoute = {
    path: "/user",
    component: User,
    children: [UserSettingsRoute]
}