/**
 * me.me.io Frontend Main
 */
const app = new Vue({
    el: "#memeio_app",
    data: {
        message: "Welcome to me.me.io!"
    },
    methods: {
        /**
         * Called when create post button is clicked.
         */
        onCreatePostButtonClick() {
            this.$router.push({
                name: "post_create"
            })
        },
        /**
         * Called when user settings button is clicked.
         */
        onUserSettingsButtonClick() {
            this.$router.push({
                name: "user_settings"
            })
        },
        /**
         * Gets called when logout button is clicked.
         */
        onLogoutButtonClick() {
            axios.post("/auth/logout")
                .then(response => {
                    this.$store.dispatch("logout")
                    this.$router.replace({
                        name: "logout"
                    })
                })
                .catch(response => {
                    this.$store.dispatch("logout")
                    this.$router.replace({
                        name: "logout"
                    })
                })
        }
    },
    watch: {
        $route(to, from) {
            const unauthenticated = ["login", "register", "logout", "notfound"]
            if (!this.$store.getters.isAuthenticated &&
                !unauthenticated.includes(to.name)) {
                this.$router.replace({
                    name: "login"
                })
            }
        }
    },
    mounted() {
        if (!this.$store.getters.isAuthenticated) {
            this.$router.replace({
                name: "login"
            })
        }
    },
    router,
    store
})