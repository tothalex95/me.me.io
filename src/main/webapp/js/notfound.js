/**
 * Custom 404 page to display if the requested page is not found.
 */
const NotFound = {
    template: `
        <div class="container">
            <h1>HTTP 404 - Not found</h1>
            <p>
                The requested page cannot be found.
                Click <router-link :to="{ name: 'post_list' }">here</router-link> to return to the {{ $store.getters.isAuthenticated ? 'home' : 'login' }} page.
            </p>
        </div>
    `
}

const NotFoundRoute = {
    path: "/404",
    name: "notfound",
    component: NotFound
}