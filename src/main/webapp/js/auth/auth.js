/**
 * Auth module to switch between login and registration
 */
const Auth = {
    template: `
        <div class="container">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <router-link v-bind:class="{ active: $route.name === 'login' }" class="nav-link" to="login">Login</router-link>
                </li>
                <li class="nav-item">
                    <router-link v-bind:class="{ active: $route.name === 'register' }" class="nav-link" to="register">Create new account</router-link>
                </li>
            </ul>

            <router-view></router-view>
        </div>
    `
}

/**
 * Router configuration of auth module
 */
const AuthRoute = {
    path: "/auth",
    component: Auth,
    children: [LoginRoute, RegistrationRoute, LogoutRoute]
}