/**
 * Registration Component
 */
const Registration = {
    data: () => {
        return {
            /**
             * User object that contains the data of the user to be registered.
             */
            user: {
                username: "",
                password: "",
                confirmPassword: "",
                email: ""
            },
            messages: []
        }
    },
    methods: {
        /**
         * Gets called when registration form is submitted.
         */
        onSubmit() {
            /**
             * Post form data to /users.
             */
            axios.post("/users", {
                    username: this.user.username,
                    password: this.user.password,
                    email: this.user.email
                })
                /**
                 * In case of success login immediately.
                 */
                .then(response => {
                    const params = new URLSearchParams()
                    params.append("username", this.user.username)
                    params.append("password", this.user.password)

                    /**
                     * Post username and password to /login in url encoded form.
                     */
                    axios.post("/login", params, {
                            headers: {
                                "Content-Type": "application/x-www-form-urlencoded"
                            }
                        })
                        /**
                         * In case of success navigates to dashboard.
                         */
                        .then(response => {
                            this.$store.dispatch("authenticate")
                            this.$router.replace({
                                name: "post_list"
                            })
                        })
                })
                /**
                 * In case of failure displays registration error.
                 */
                .catch(response => {
                    response.response.data.errors.forEach(errorCode => {
                        this.messages.push({
                            text: errorCode,
                            type: "error"
                        })
                    })
                })
        }
    },
    template: `
        <div class="container">
            <div v-if="messages.length > 0">
                <div
                    v-for="message in messages"
                    :class="{ alert: true, 'alert-success': message.type === 'success', 'alert-danger': message.type === 'error' }">
                    {{ message.text }}
                </div>
            </div>
            <form @submit.prevent="onSubmit()">
                <div class="form-group row">
                    <label for="username" class="col-form-label">Username</label>
                    <input type="text" id="username" class="form-control" v-model="user.username" required minlength="1"
                        maxlength="16">
                </div>

                <div class="form-group row">
                    <label for="password" class="col-form-label">Password</label>
                    <input type="password" id="password" class="form-control" v-model="user.password" required minlength="3"
                        maxlength="16">
                </div>

                <div class="form-group row">
                    <label for="confirmPassword" class="col-form-label">Confirm password</label>
                    <input type="password" id="confirmPassword" class="form-control" v-model="user.confirmPassword" required
                        minlength="3" maxlength="16">
                </div>

                <div class="form-group row">
                    <label for="email" class="col-form-label">Email</label>
                    <input type="text" id="email" class="form-control" v-model="user.email" required>
                </div>

                <button type="submit" class="btn btn-primary">Register</button>
            </form>
        </div>
    `
}

/**
 * Router configuration of registration component
 */
const RegistrationRoute = {
    path: "register",
    name: "register",
    component: Registration
}