/**
 * Logout Component
 */
const Logout = {
    template: `
        <div class="container">
            <h2>You've successfully logged out.</h2>
        </div>
    `
}

/**
 * Router configuration of logout component
 */
const LogoutRoute = {
    path: "logout",
    name: "logout",
    component: Logout
}