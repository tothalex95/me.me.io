/**
 * Login Component
 */
const Login = {
    data: () => {
        return {
            /**
             * User object that contains the username and the password.
             */
            user: {
                username: "",
                password: ""
            },
            loginError: false
        }
    },
    methods: {
        /**
         * Gets called when login form is submitted.
         */
        onSubmit() {
            const params = new URLSearchParams()
            params.append("username", this.user.username)
            params.append("password", this.user.password)

            /**
             * Post username and password to /login in url encoded form.
             */
            axios.post("/login", params, {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                })
                /**
                 * In case of success navigates to dashboard.
                 */
                .then(response => {
                    this.$store.dispatch("authenticate")
                    this.$router.replace({
                        name: "post_list"
                    })
                })
                /**
                 * In case of failure displays login error.
                 */
                .catch(response => {
                    this.loginError = true
                })
        }
    },
    template: `
        <div class="container">
            <div class="alert alert-danger" v-if="loginError">Failed to log in.</div>
            <form @submit.prevent="onSubmit()">
                <div class="form-group row">
                    <label for="username" class="col-form-label">Username</label>
                    <input type="text" id="username" class="form-control" v-model="user.username" required minlength="3"
                        maxlength="16">
                </div>

                <div class="form-group row">
                    <label for="password" class="col-form-label">Password</label>
                    <input type="password" id="password" class="form-control" v-model="user.password" required minlength="8"
                        maxlength="16">
                </div>

                <button type="submit" class="btn btn-primary">Login</button>
            </form>
        </div>
    `
}

/**
 * Router configuration of login component
 */
const LoginRoute = {
    path: "login",
    name: "login",
    component: Login
}