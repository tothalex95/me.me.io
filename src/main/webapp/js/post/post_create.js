/**
 * Post Create Component
 */
const PostCreate = {
    props: {
        parent: {
            type: Number,
            required: false,
            default: null
        }
    },
    data() {
        return {
            /**
             * Post object that contains the title, text and image of the post.
             */
            post: {
                title: null,
                text: null,
                parentId: this.parent
            },
            messages: []
        }
    },
    methods: {
        /**
         * Called when post create form is submitted.
         */
        onSubmit() {
            const formData = new FormData()
            formData.append("post", new Blob([JSON.stringify(this.post)], {
                type: "application/json"
            }))
            formData.append("image", document.getElementById("image").files[0])

            // Post the title and the content as multipart form data.
            axios.post("/posts", formData, {
                headers: {
                    "Content-Type": undefined
                }
            })
            /*
             * In case of success:
             * If it is an original post, navigate to post view.
             * If it is a comment, reset the form and emit event to the parent component.
             */
            .then(response => {
                if (this.parent) {
                    this.resetForm()
                    this.$emit("replied")
                    return
                }
                this.$router.replace({
                    name: "post_view",
                    params: {
                        postId: response.data.id
                    }
                })
            })
            // In case of failure, display error message.
            .catch(response => {
                response.response.data.errors.forEach(errorCode => {
                    this.messages.push({
                        text: errorCode,
                        type: "error"
                    })
                })
            })
        },
        /**
         * Reset form data.
         */
        resetForm() {
            this.post = {
                title: null,
                text: null,
                parentId: this.parent
            }
        }
    },
    template: `
        <div class="container">
            <div v-if="messages.length > 0">
                <div
                    v-for="message in messages"
                    :class="{ alert: true, 'alert-success': message.type === 'success', 'alert-danger': message.type === 'error' }">
                    {{ message.text }}
                </div>
            </div>
            <form @submit.prevent="onSubmit()" name="post_create_form">
                <div class="form-group" v-if="!parent">
                    <label for="title" class="col-form-label">Title</label>
                    <input type="text" id="title" class="form-control" v-model="post.title" required>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="text" class="col-form-label">Text</label>
                        <textarea class="form-control" id="text" v-model="post.text"></textarea>
                    </div>
                    <div class="form-group col">
                        <label for="image" class="col-form-label">Image</label>
                        <input type="file" id="image" class="form-control-file" accept=".png, .jpg, .gif">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    `
}

/**
 * Register post create as a global component as it will be used in others.
 */
Vue.component("post-create", PostCreate)

/**
 * Router configuration of post create component
 */
const PostCreateRoute = {
    path: "create",
    name: "post_create",
    component: PostCreate
}