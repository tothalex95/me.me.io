/**
 * Post List Component
 */
const PostList = {
    data() {
        return {
            posts: null
        }
    },
    created() {
        // Send an HTTP GET request to retrieve posts.
        axios.get("/posts", {
                params: {}
            })
            .then(response => {
                this.posts = response.data.posts
            })
    },
    template: `
        <div class="container">
            <div v-if="!posts" class="alert alert-warning">There are no posts yet.</div>
            <post-view v-if="posts" v-for="post in posts" :post="post"></post-view>
        </div>
    `
}

/**
 * Router configuration of post list component
 */
const PostListRoute = {
    path: "list",
    name: "post_list",
    component: PostList
}