/**
 * Post View Component
 */
const PostView = {
    props: {
        /**
         * Post object that contains basic post data necessary for an overview.
         */
        post: {
            type: Object,
            required: false,
            default: null
        }
    },
    data() {
        return {
            /**
             * The actual post object being displayed.
             * It's the same as the post object received as property if any.
             * Otherwise it's retrieved from the server based on the post id.
             */
            actualPost: this.post,
            /**
             * Indicate whether the corresponding comments should be retrieved from the server.
             */
            showComments: !this.post,
            /**
             * Comments related to the actual post.
             * Retrieved from the backend if showComments flag is set to true.
             */
            comments: null,
            /**
             * Indicate whether reply form should be displayed.
             */
            showReplyForm: false
        }
    },
    computed: {
        /**
         * Return a date string created from the elements of the post's lastModified array.
         */
        formattedLastModified() {
            const lastModified = this.actualPost.lastModified.map(n => n < 10 ? "0" + n : n.toString())
            return [lastModified.slice(0, 3).join("."), lastModified.slice(3).join(":")].join(". ")
        }
    },
    created() {
        /**
         * If $route.params.postId is present, retrieve the corresponding post data from backend.
         * Otherwise this component is loaded on another route (e.g. list), so it receives post data as property.
         */
        if (!this.actualPost) {
            this.loadPost()
        }
    },
    methods: {
        /**
         * Called when upvote button is clicked.
         */
        onUpvoteButtonClicked() {
            console.log("Upvote button clicked. Backend is not ready yet.")
        },
        /**
         * Called when downvote button is clicked.
         */
        onDownvoteButtonClicked() {
            console.log("Downvote button clicked. Backend is not ready yet.")
        },
        /**
         * Called when show comments button is clicked.
         */
        onShowCommentsButtonClicked() {
            // If the current route is the post view route, simply display comments.
            if (this.$route.name === PostViewRoute.name) {
                this.loadComments()
                this.showComments = true
            }
            // Otherwise, navigate to post view.
            else {
                this.$router.push({
                    name: "post_view",
                    params: {
                        postId: this.actualPost.id
                    }
                })
            }
        },
        /**
         * Called when reply button is clicked or reply form is closed.
         */
        onReplyButtonClicked() {
            this.showReplyForm = !this.showReplyForm
        },
        /**
         * Called when reply is submitted.
         * Triggers onShowCommentsButtonClicked and onReplyButtonClicked.
         */
        onReplySubmitted() {
            this.onShowCommentsButtonClicked()
            this.onReplyButtonClicked()
        },
        /**
         * Retrieve post (or comment) data based on post id from the server.
         */
        loadPost() {
            axios.get(`/posts/${this.$route.params.postId}`, {
                    params: {}
                })
                .then(response => {
                    this.actualPost = response.data
                    if (this.showComments) {
                        this.loadComments()
                    }
                })
        },
        /**
         * Retrieve comments related to the current post (or comment) from the server.
         */
        loadComments() {
            let postId = this.actualPost.parentId ? this.actualPost.id : this.$route.params.postId
            axios.get(`/posts/${postId}/comments`, {
                    params: {}
                })
                .then(response => this.comments = response.data.comments)
        }
    },
    template: `
        <div class="container" v-if="actualPost">
            <div class="card">
                <div class="card-header" v-if="!actualPost.parentId">
                    <router-link :to="{ name: 'post_view', params: { postId: actualPost.id } }">
                        {{ actualPost.title }}
                    </router-link>
                </div>
                <div class="card-body">
                    <p class="card-text" v-if="actualPost.text">{{ actualPost.text }}</p>
                    <img class="rounded" v-if="actualPost.imageType" v-bind:src="'/posts/' + actualPost.id + '/image'" width="400" height="400">
                    <p class="card-text">
                        <small class="text-muted">
                            Last modified by {{ actualPost.authorUsername }} on {{ formattedLastModified }}
                        </small>
                    </p>
                </div>
                <div class="card-footer">
                    <div class="btn-group" role="group" aria-label="Up- & Downvote buttons">
                        <button class="btn btn-success" @click="onUpvoteButtonClicked">Upvote</button>
                        <button class="btn btn-danger" @click="onDownvoteButtonClicked">Downvote</button>
                    </div>
                    <button class="btn btn-info" @click="onReplyButtonClicked" v-if="!showReplyForm">
                        Reply
                    </button>
                    <button class="btn btn-info float-right" @click="onShowCommentsButtonClicked" v-if="!showComments && actualPost.hasChild">
                        Show comments
                    </button>
                </div>
            </div>
            <div class="alert alert-dark alert-dismissable fade show" role="alert" v-if="showReplyForm">
                <button type="button" class="close" data-dismiss="alert" aria-label="close" @click="onReplyButtonClicked">
                    <span aria-hidden="true">&times;</span>
                </button>
                <post-create :parent="actualPost.id" @replied="onReplySubmitted"></post-create>
            </div>
            <div class="list-group" v-if="comments && showComments">
                <li class="list-group-item" v-for="comment in comments">
                    <post-view :post="comment"></post-view>
                </li>
            </div>
        </div>
    `
}

/**
 * Register post view as a global component as it will be used in others.
 */
Vue.component("post-view", PostView)

/**
 * Router configuration of post view component
 */
const PostViewRoute = {
    path: "view/:postId",
    name: "post_view",
    component: PostView
}