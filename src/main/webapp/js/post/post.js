/**
 * Post Component
 */
const Post = {
    template: `
        <div class="container">
            <router-view></router-view>
        </div>
    `
}

/**
 * Router configuration of post component
 */
const PostRoute = {
    path: "/post",
    component: Post,
    children: [PostCreateRoute, PostViewRoute, PostListRoute]
}