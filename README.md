[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=tothalex95_me.me.io&metric=alert_status)](https://sonarcloud.io/dashboard?id=tothalex95_me.me.io)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=tothalex95_me.me.io&metric=coverage)](https://sonarcloud.io/dashboard?id=tothalex95_me.me.io)

# me.me.io

## Getting Started

### Guides
The following guides illustrates how to use certain features concretely:

* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Accessing data with MySQL](https://spring.io/guides/gs/accessing-data-mysql/)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)

